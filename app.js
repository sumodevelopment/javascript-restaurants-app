import Restaurant from './restaurant.js'

class App {
    constructor() {
        this.elRestaurantList
        this.restaurant
        return this
    }
}

App.prototype.init = function() {
    // Render the initial state.
    this.elRestaurantList = document.getElementById('restaurant-list')
    this.restaurant = new Restaurant( this.elRestaurantList )
    this.render();
}

App.prototype.render = function() {
    this.restaurant.renderRestaurantListHTML();
}

new App().init()

