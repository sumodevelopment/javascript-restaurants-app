export const MOCK_RESTAURANTS = [
    {
        id: 1,
        name: 'Geranium',
        description: 'High-concept cooking with multiple course tasting menus, plus panoramic views from the 8th floor.',
        openingHours: '9:00 - 16:00',
        address: {
            street: 'Per Henrik Lings Allé 4, 8. Sal, 2100 København',
            country: 'Denmark',
            coords: [55.703748, 11.4515858]
        },
        image: 'https://images.unsplash.com/photo-1555396273-367ea4eb4db5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1867&q=80'
    },
    {
        id: 2,
        name: 'Noma',
        description: 'Chef Rene Redzepi\'s gastronomic mecca, creating world-class dishes presented in 20-course meals.',
        openingHours: '12:00 - 22:00',
        address: {
            street: 'Refshalevej 96, 1432 København K',
            country: 'Denmark',
            coords: [55.7050252, 12.3809029]
        },
        image: 'https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80'
    },
    {
        id: 3,
        name: 'Restaurant Barr',
        description: 'Chef Rene Redzepi\'s gastronomic mecca, creating world-class dishes presented in 20-course meals.',
        openingHours: '12:00 - 22:00',
        address: {
            street: 'Strandgade 93, 1401 København',
            country: 'Denmark',
            coords: [55.6772515,12.545132]
        },
        image: 'https://images.unsplash.com/photo-1514933651103-005eec06c04b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1867&q=80'
    },
    {
        id: 4,
        name: 'Restaurant Els',
        description: 'Refined cooking in a classy restaurant with elegant 19th-century decor boasting 6 delicate murals.',
        openingHours: '16:00 - 23:00',
        address: {
            street: 'Store Strandstræde 3, 1255 København',
            country: 'Denmark',
            coords: [55.7251214, 12.3934399]
        },
        image: 'https://images.unsplash.com/photo-1549488344-1f9b8d2bd1f3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80'
    },
    {
        id: 5,
        name: 'Kokkeriet',
        description: 'Creative contemporary seasonal cooking in a polished upscale restaurant with a cool jazz soundtrack.',
        openingHours: '11:00 - 00:00',
        address: {
            street: 'Kronprinsessegade 64, 1306 København K',
            country: 'Denmark',
            coords: [55.7212047, 12.5058983]
        },
        image: 'https://images.unsplash.com/photo-1549488344-1f9b8d2bd1f3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80'
    },
];

