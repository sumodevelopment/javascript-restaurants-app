const RestaurantDetailStyles = {
    position: 'fixed',
    top: '52px',
    backgroundColor: 'var(--col-dark)',
    transform: 'translateY(100%)',
    width: '100%',
    height: '100%',
    animationName: 'slideInUp',
    animationDuration: '350ms',
    animationTimingFunction: 'ease-in-out',
    animationFillMode: 'forwards',
    overflowY: 'auto',
    maxWidth: '30em'
}

const ContentContainerStyle = {
    padding: '2em'
}

const CloseButtonStyle = {
    width: '55px',
    height: '55px',
    borderRadius: '50%',
    backgroundColor: 'transparent',
    color: 'white',
    position: 'fixed',
    top: '1em', // 16px - Default Document Font Size
    right: '1em',
    border: '2px solid white',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
}

class RestaurantDetail {

    constructor(restaurant) {
        this.restaurant = restaurant
        this.elRestaurantDetail = null
    }

    close() {
        // Animate first. Then Remove from DOM.
        this.elRestaurantDetail.addEventListener('animationend', () => {
            this.elRestaurantDetail.remove()
            this.elRestaurantDetail = null
        })

        this.elRestaurantDetail.style.animationName = 'slideOutDown'
    }

    renderRestaurantDetail() {

        this.elRestaurantDetail = document.createElement('article')
        Object.assign(this.elRestaurantDetail.style, RestaurantDetailStyles)
        
        const elCloseBtn = document.createElement('button')
        Object.assign(elCloseBtn.style, CloseButtonStyle)
        elCloseBtn.innerHTML = `<i class="material-icons">close</i>`
        elCloseBtn.addEventListener('click', this.close.bind(this))
        this.elRestaurantDetail.appendChild(elCloseBtn)

        const elImageContainer = document.createElement('header');
        this.elRestaurantDetail.appendChild(elImageContainer);

        const elImage = new Image();
        elImage.onload = () => {
            elImageContainer.appendChild(elImage)
        }
        elImage.src = this.restaurant.image;

        const elContentSection = document.createElement('section')
        Object.assign(elContentSection.style, ContentContainerStyle)

        const elName = document.createElement('h1')
        elName.className = 'title has-text-white'
        elName.innerText = this.restaurant.name
        elContentSection.appendChild(elName)

        const elDescription = document.createElement('p')
        elDescription.innerText = this.restaurant.description
        elContentSection.appendChild(elDescription)

        const elAddress = document.createElement('address')
        elAddress.innerHTML = `
            <br>
            <h4 class="subtitle has-text-white">
                <i class="material-icons">location_on</i> Address
            <h4>
            ${this.restaurant.address.street} <br />
            ${this.restaurant.address.country} <br />
        `
        elContentSection.appendChild(elAddress)

        this.elRestaurantDetail.appendChild( elContentSection )

        document.body.appendChild(this.elRestaurantDetail)

    }

}

export default RestaurantDetail