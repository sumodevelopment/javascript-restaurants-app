import { MOCK_RESTAURANTS } from './restaurants.mock.js'
import RestaurantDetail from './restaurant-detail.js'

class Restaurant {
    
    constructor(restaurantListElement) {
        this.restaurants = MOCK_RESTAURANTS
        this.elRestaurantList = restaurantListElement
    }

    renderRestaurantDetail(restaurant) {
        // this = Restaurant
        const restaurantDetail = new RestaurantDetail( restaurant )
        restaurantDetail.renderRestaurantDetail()
    }

    renderRestaurantListHTML() {
        this.restaurants.forEach(restaurant => {
            // Create a new rest element and add it to the elRest.
            const elRestaurant = document.createElement('li')
            elRestaurant.className = "restaurant-list__item"

            const elImageContainer = document.createElement('aside');
            elImageContainer.className = "restaurant-list__item-image__container"
            elRestaurant.appendChild(elImageContainer);

            const elImage = new Image();
            elImage.className = "restaurant-list__item-image"
            elImage.onload = function () {
                elImageContainer.appendChild(elImage);
            }
            elImage.src = restaurant.image;

            const elContent = document.createElement('div')
            elContent.className = "restaurant-list__item-content"

            const elName = document.createElement('h4');
            elName.innerText = restaurant.name;
            elName.className = "title has-text-white is-size-5 mb-0 restaurant-list__item-name"
            elContent.appendChild(elName);

            const elDescription = document.createElement('p');
            elDescription.innerText = restaurant.description;
            elDescription.className = "restaurant-list__item-description"
            elContent.appendChild(elDescription);

            elRestaurant.addEventListener('click', () => {
                this.renderRestaurantDetail( restaurant )
            });

            elRestaurant.appendChild(elContent)
            this.elRestaurantList.appendChild(elRestaurant)
        });
    }
}

export default Restaurant;